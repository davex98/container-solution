package app_test

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/davex98/container-solution/internal/titanic/adapters"
	"gitlab.com/davex98/container-solution/internal/titanic/app"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"math/rand"
	"testing"
)

func TestService(t *testing.T) {

	t.Run("Service test", func(t *testing.T) {
		t.Parallel()

		t.Run("Adding New Person", func(t *testing.T) {
			t.Parallel()
			testAddingPerson(t)
		})

		t.Run("Deleting Person", func(t *testing.T) {
			t.Parallel()
			testDeletingPerson(t)
		})

		t.Run("Updating Person", func(t *testing.T) {
			t.Parallel()
			testUpdatingPerson(t)
		})

		t.Run("Getting all people", func(t *testing.T) {
			t.Parallel()
			testGettingPeople(t)
		})
	})
}

func testAddingPerson(t *testing.T) {
	ctx := context.Background()
	repo := adapters.NewInMemoryRepo()
	service := app.NewService(repo)

	data := getRandomPersonData(t)
	person1, err := service.AddPerson(ctx, data)
	require.NoError(t, err)
	perons2, err := service.GetPerson(ctx, person1.UUID)
	require.NoError(t, err)
	require.Equal(t, person1, perons2)
}

func testDeletingPerson(t *testing.T) {
	ctx := context.Background()
	repo := adapters.NewInMemoryRepo()
	service := app.NewService(repo)

	data := getRandomPersonData(t)
	person, err := service.AddPerson(ctx, data)
	require.NoError(t, err)
	err = service.DeletePerson(ctx, person.UUID)
	require.NoError(t, err)
	newPerson, err := service.GetPerson(ctx, person.UUID)
	require.Equal(t, err, domain.InvalidPerson)
	require.True(t, newPerson.IsEmpty())
}

func testUpdatingPerson(t *testing.T) {
	ctx := context.Background()
	repo := adapters.NewInMemoryRepo()
	service := app.NewService(repo)

	data := getRandomPersonData(t)
	person, err := service.AddPerson(ctx, data)
	require.NoError(t, err)

	data = domain.PersonData{
		Age:                     rand.Float32(),
		Fare:                    0,
		Name:                    "Updated",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}

	updatedPerson, err := service.UpdatePerson(ctx, person.UUID, data)
	require.NoError(t, err)
	require.Equal(t, updatedPerson.Name, "Updated")
	require.Equal(t, updatedPerson.UUID, person.UUID)
}

func testGettingPeople(t *testing.T)  {
	ctx := context.Background()
	repo := adapters.NewInMemoryRepo()
	service := app.NewService(repo)

	d1 := getRandomPersonData(t)
	person1, err := service.AddPerson(ctx, d1)
	require.NoError(t, err)
	d2 := getRandomPersonData(t)
	person2, err := service.AddPerson(ctx, d2)
	require.NoError(t, err)

	people, err := repo.GetPeople(ctx)
	require.True(t, containsPerson(people, person1))
	require.True(t, containsPerson(people, person2))
}

func getRandomPersonData(t *testing.T) domain.PersonData {
	t.Helper()

	return domain.PersonData{
		Age:                     rand.Float32(),
		Fare:                    0,
		Name:                    "Updated",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
}

func containsPerson(people []domain.Person, person domain.Person) bool {
	for _, p := range people {
		if p == person {
			return true
		}
	}
	return false
}