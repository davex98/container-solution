package app

import (
	"context"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
)

type Service struct {
	repo domain.Repository
}

func NewService(repo domain.Repository) Service {
	if repo == nil {
		panic("empty repo")
	}
	return Service{repo: repo}
}

func (s Service) GetPeople(ctx context.Context) ([]domain.Person, error){
	people, err := s.repo.GetPeople(ctx)
	if err != nil {
		return nil, err
	}
	return people, nil
}

func (s Service) AddPerson(ctx context.Context, data domain.PersonData) (domain.Person, error) {
	person, err := s.repo.AddPerson(ctx, data)
	if err != nil {
		return domain.Person{}, err
	}
	return person, nil
}

func (s Service) GetPerson(ctx context.Context, uuid string) (domain.Person, error) {
	person, err := s.repo.GetPerson(ctx, uuid)
	if err != nil {
		return domain.Person{}, err
	}
	return person, nil
}

func (s Service) DeletePerson(ctx context.Context, uuid string) error {
	err := s.repo.DeletePerson(ctx, uuid)
	if err != nil {
		return err
	}
	return nil
}

func (s Service) UpdatePerson(ctx context.Context, uuid string, data domain.PersonData) (domain.Person, error) {
	person, err := s.repo.UpdatePerson(ctx, uuid, data)
	if err != nil {
		return domain.Person{}, err
	}
	return person, nil
}