package main

import (
	"context"
	"github.com/go-chi/chi"
	"gitlab.com/davex98/container-solution/internal/common/log"
	"gitlab.com/davex98/container-solution/internal/common/server"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/davex98/container-solution/internal/titanic/adapters"
	"gitlab.com/davex98/container-solution/internal/titanic/app"
	"gitlab.com/davex98/container-solution/internal/titanic/ports"
	"net/http"
)

func main() {
	log.Init()
	ctx := context.Background()

	conn, err := adapters.NewPostgresConnection(ctx)
	if err != nil {
		panic(err)
	}
	repository := adapters.NewPostgresRepository(conn)
	application := app.NewService(repository)

	server.RunHTTPServer(func(router chi.Router) http.Handler {
		router.Handle("/metrics", promhttp.Handler())
		return ports.HandlerFromMux(
			ports.NewHttpServer(application),
			router,
		)
	})
}