package domain


type Person struct {
	PersonData
	UUID string `bson:"uuid"`
}

func (p Person) IsEmpty() bool {
	return p == Person{}
}

type PersonData struct {
	Age                     float32     `bson:"age"`
	Fare                    float32 `bson:"fare"`
	Name                    string  `bson:"name"`
	ParentsOrChildrenAboard int     `bson:"parentsOrChildrenAboard"`
	PassengerClass          int     `bson:"passengerClass"`
	Sex                     string  `bson:"sex"`
	SiblingsOrSpousesAboard int     `bson:"siblingsOrSpousesAboard"`
	Survived                bool    `bson:"survived"`
}
