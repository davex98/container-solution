package domain

import (
	"context"
	"github.com/pkg/errors"
)

type Repository interface {
	GetPeople(ctx context.Context) ([]Person, error)
	GetPerson(ctx context.Context, uuid string) (Person, error)
	DeletePerson(ctx context.Context, uuid string) error
	AddPerson(ctx context.Context, person PersonData) (Person, error)
	UpdatePerson(ctx context.Context, uuid string, person PersonData) (Person, error)
}

var (
	InvalidPerson = errors.New("this person does not exist")
)
