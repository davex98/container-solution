package ports_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/davex98/container-solution/internal/titanic/adapters"
	"gitlab.com/davex98/container-solution/internal/titanic/app"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"gitlab.com/davex98/container-solution/internal/titanic/ports"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
)


func TestHttpServer(t *testing.T) {

	t.Run("Http server test", func(t *testing.T) {
		t.Parallel()

		t.Run("Add new person /POST /people", func(t *testing.T) {
			t.Parallel()
			testServerAddPerson(t)
		})

		t.Run("Get all people /GET /people", func(t *testing.T) {
			t.Parallel()
			testServerGetAllPeople(t)
		})

		t.Run("Get person /GET /people/{uuid}", func(t *testing.T) {
			t.Parallel()
			testServerGetPerson(t)
			testServerGetInvalidPerson(t)
		})

		t.Run("Delete person /DELETE /people/{uuid}", func(t *testing.T) {
			t.Parallel()
			testServerDeletePerson(t)
			testServerDeleteInvalidPerson(t)
		})

		t.Run("Update person /PUT /people/{uuid}", func(t *testing.T) {
			t.Parallel()
			testServerUpdatePerson(t)
			testServerUpdateInvalidPerson(t)
		})
	})
}

func testServerAddPerson(t *testing.T) {
	dep := newDependencies()

	randomPerson := getRandomPersonData(t)
	jsonStr := getByteFromPersonData(randomPerson)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/people", srv.URL), bytes.NewBuffer(jsonStr))
	require.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusOK)

	all, err := ioutil.ReadAll(res.Body)
	require.NoError(t, err)

	var responsePerson ports.Person
	data := bytes.TrimSpace(all)
	err = json.Unmarshal(data, &responsePerson)
	require.NoError(t, err)

	person, err := dep.repo.GetPerson(req.Context(), *responsePerson.Uuid)
	require.NoError(t, err)
	require.Equal(t, person, getDomainPersonFromResponse(responsePerson))
}

func testServerGetPerson(t *testing.T) {
	dep := newDependencies()

	person, err := dep.repo.AddPerson(context.TODO(), getRandomPersonData(t))
	require.NoError(t, err)


	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	res, err := http.Get(fmt.Sprintf("%s/people/%s", srv.URL, person.UUID))
	require.NoError(t, err)

	require.Equal(t, res.StatusCode, http.StatusOK)

	all, err := ioutil.ReadAll(res.Body)
	require.NoError(t, err)

	var responsePerson ports.Person
	data := bytes.TrimSpace(all)
	err = json.Unmarshal(data, &responsePerson)
	require.NoError(t, err)
	require.Equal(t, person, getDomainPersonFromResponse(responsePerson))
}

func testServerGetInvalidPerson(t *testing.T) {
	dep := newDependencies()

	_, err := dep.repo.AddPerson(context.TODO(), getRandomPersonData(t))
	require.NoError(t, err)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	res, err := http.Get(fmt.Sprintf("%s/people/%s", srv.URL, uuid.New().String()))
	require.NoError(t, err)

	require.Equal(t, res.StatusCode, http.StatusNotFound)
}

func testServerDeletePerson(t *testing.T) {
	dep := newDependencies()

	person, err := dep.repo.AddPerson(context.TODO(), getRandomPersonData(t))
	require.NoError(t, err)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	request, err := http.NewRequest("DELETE", fmt.Sprintf("%s/people/%s", srv.URL, person.UUID), nil)
	require.NoError(t, err)
	res, err := http.DefaultClient.Do(request)
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusOK)

	require.NoError(t, err)
	people, err := dep.repo.GetPeople(context.TODO())
	require.NoError(t, err)
	require.Equal(t, 0, len(people))
}

func testServerDeleteInvalidPerson(t *testing.T) {
	dep := newDependencies()

	_, err := dep.repo.AddPerson(context.TODO(), getRandomPersonData(t))
	require.NoError(t, err)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	request, err := http.NewRequest("DELETE", fmt.Sprintf("%s/people/%s", srv.URL, uuid.New().String()), nil)
	require.NoError(t, err)
	res, err := http.DefaultClient.Do(request)
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusNotFound)
}



func testServerUpdatePerson(t *testing.T) {
	dep := newDependencies()

	randomPerson := getRandomPersonData(t)
	person, err := dep.repo.AddPerson(context.TODO(), randomPerson)
	require.NoError(t, err)
	randomPerson.Name = "Updated Name"
	jsonStr := getByteFromPersonData(randomPerson)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/people/%s", srv.URL, person.UUID), bytes.NewBuffer(jsonStr))
	require.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusOK)

	require.NoError(t, err)
	people, err := dep.repo.GetPerson(context.TODO(), person.UUID)
	require.NoError(t, err)
	require.Equal(t, "Updated Name", people.Name)
}

func testServerUpdateInvalidPerson(t *testing.T) {
	dep := newDependencies()

	randomPerson := getRandomPersonData(t)
	_, err := dep.repo.AddPerson(context.TODO(), randomPerson)
	require.NoError(t, err)
	jsonStr := getByteFromPersonData(randomPerson)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/people/%s", srv.URL, uuid.New().String()), bytes.NewBuffer(jsonStr))
	require.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusNotFound)
}

func testServerGetAllPeople(t *testing.T) {
	dep := newDependencies()
	p1 := getRandomPersonData(t)
	p2 := getRandomPersonData(t)
	person1, err := dep.repo.AddPerson(context.TODO(), p1)
	require.NoError(t, err)
	person2, err := dep.repo.AddPerson(context.TODO(), p2)
	require.NoError(t, err)

	srv := httptest.NewServer(ports.HandlerFromMux(dep.server, chi.NewMux()))
	res, err := http.Get(fmt.Sprintf("%s/people", srv.URL))
	require.NoError(t, err)
	require.Equal(t, res.StatusCode, http.StatusOK)

	all, err := ioutil.ReadAll(res.Body)
	require.NoError(t, err)

	var responsePeople ports.People
	data := bytes.TrimSpace(all)
	err = json.Unmarshal(data, &responsePeople)
	require.NoError(t, err)
	require.Equal(t, 2, len(responsePeople))
	require.True(t, containsPerson(responsePeople, person1))
	require.True(t, containsPerson(responsePeople, person2))
}

func containsPerson(people ports.People, person domain.Person) bool {
	for _, p := range people {
		if getDomainPersonFromResponse(p) == person {
			return true
		}
	}
	return false
}

type dependencies struct {
	repo domain.Repository
	app app.Service
	server ports.ServerInterface
}

func newDependencies() dependencies {
	repo := adapters.NewInMemoryRepo()
	app := app.NewService(repo)
	server := ports.NewHttpServer(app)
	return dependencies{
		repo:   repo,
		app:    app,
		server: server,
	}
}

func getRandomPersonData(t *testing.T) domain.PersonData {
	t.Helper()

	return domain.PersonData{
		Age:                     rand.Float32(),
		Fare:                    0,
		Name:                    "random person",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
}

func getDomainPersonFromResponse(data ports.Person) domain.Person {
	return domain.Person{
		PersonData: domain.PersonData{
			Age:                     *data.Age,
			Fare:                    *data.Fare,
			Name:                    *data.Name,
			ParentsOrChildrenAboard: *data.ParentsOrChildrenAboard,
			PassengerClass:          *data.PassengerClass,
			Sex:                     *data.Sex,
			SiblingsOrSpousesAboard: *data.SiblingsOrSpousesAboard,
			Survived:                *data.Survived,
		},
		UUID:       *data.Uuid,
	}
}

func getByteFromPersonData(data domain.PersonData) []byte {
	s := fmt.Sprintf(`{
					  "survived": %t,
					  "passengerClass": %v,
					  "name": "%s",
                      "sex": "%s",
                      "age": %v,
					  "siblingsOrSpousesAboard": %v,
					  "parentsOrChildrenAboard": %v,
					  "fare": %v
						}`, data.Survived, data.PassengerClass, data.Name, data.Sex, data.Age, data.SiblingsOrSpousesAboard, data.ParentsOrChildrenAboard, data.Fare)
	return []byte(s)
}