package ports

import (
	"fmt"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/davex98/container-solution/internal/titanic/app"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"net/http"
)

type HttpServer struct {
	app app.Service
}

func NewHttpServer(app app.Service) ServerInterface {
	return HttpServer{app: app}
}

func (h HttpServer) ListPeople(w http.ResponseWriter, r *http.Request) {
	people, err := h.app.GetPeople(r.Context())
	if err != nil {
		handleError(w, r, err)
		return
	}
	peopleResp := domainPeopleToResponse(people)
	render.Respond(w, r, peopleResp)
}


func (h HttpServer) AddPerson(w http.ResponseWriter, r *http.Request) {
	addPerson := AddPersonJSONBody{}
	if err := render.Decode(r, &addPerson); err != nil {
		handleError(w, r, err)
		return
	}
	data := responseDataToDomain(PersonData(addPerson))

	person, err := h.app.AddPerson(r.Context(), data)
	if err != nil {
		handleError(w, r, err)
		return
	}
	personResponse := domainPersonToResponse(person)
	render.Respond(w, r, personResponse)
}

func (h HttpServer) DeletePerson(w http.ResponseWriter, r *http.Request, uuid string) {
	valid := validateUUID(w, r, uuid)
	if !valid {
		return
	}
	err := h.app.DeletePerson(r.Context(), uuid)
	if err != nil {
		handleError(w, r, err)
		return
	}
}

func (h HttpServer) GetPerson(w http.ResponseWriter, r *http.Request, uuid string) {
	valid := validateUUID(w, r, uuid)
	if !valid {
		return
	}
	person, err := h.app.GetPerson(r.Context(), uuid)
	if err != nil {
		handleError(w, r, err)
		return
	}
	personResponse := domainPersonToResponse(person)
	render.Respond(w, r, personResponse)
}

func (h HttpServer) UpdatePerson(w http.ResponseWriter, r *http.Request, uuid string) {
	valid := validateUUID(w, r, uuid)
	if !valid {
		return
	}
	updatePerson := UpdatePersonJSONBody{}
	if err := render.Decode(r, &updatePerson); err != nil {
		handleError(w, r, err)
		return
	}

	data := responseDataToDomain(PersonData(updatePerson))

	person, err := h.app.UpdatePerson(r.Context(), uuid, data)
	if err != nil {
		handleError(w, r, err)
		return
	}
	personResponse := domainPersonToResponse(person)
	render.Respond(w, r, personResponse)
}

func domainPeopleToResponse(data []domain.Person) People {
	var people []Person
	for _, p := range data {
		person := domainPersonToResponse(p)
		people = append(people, person)
	}
	return people
}

func domainPersonToResponse(p domain.Person) Person {
	person := Person{
		PersonData: PersonData{
			Age:                     &p.Age,
			Fare:                    &p.Fare,
			Name:                    &p.Name,
			ParentsOrChildrenAboard: &p.ParentsOrChildrenAboard,
			PassengerClass:          &p.PassengerClass,
			Sex:                     &p.Sex,
			SiblingsOrSpousesAboard: &p.SiblingsOrSpousesAboard,
			Survived:                &p.Survived,
		},
		Uuid:       &p.UUID,
	}

	return person
}

func responseDataToDomain(data PersonData) domain.PersonData {
	return domain.PersonData{
		Age:                     *data.Age,
		Fare:                    *data.Fare,
		Name:                    *data.Name,
		ParentsOrChildrenAboard: *data.ParentsOrChildrenAboard,
		PassengerClass:          *data.PassengerClass,
		Sex:                     *data.Sex,
		SiblingsOrSpousesAboard: *data.SiblingsOrSpousesAboard,
		Survived:                *data.Survived,
	}
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	if errors.Is(err, domain.InvalidPerson) {
		w.WriteHeader(http.StatusNotFound)
		resp := ErrorResponse{err.Error(), http.StatusNotFound}
		render.Respond(w, r, resp)
		return
	}
	w.WriteHeader(http.StatusInternalServerError)
	resp := ErrorResponse{err.Error(), http.StatusInternalServerError}
	render.Respond(w, r, resp)
}

type ErrorResponse struct {
	ErrorMessage       string `json:"errorMessage"`
	HttpStatus int
}

func validateUUID(w http.ResponseWriter, r *http.Request, id string) bool {
	_, err := uuid.Parse(id)

	if err != nil {
		err := errors.New(fmt.Sprintf("invalid uuid: %s", id))
		w.WriteHeader(http.StatusBadRequest)
		resp := ErrorResponse{err.Error(), http.StatusBadRequest}
		render.Respond(w, r, resp)
		return false
	}
	return true
}