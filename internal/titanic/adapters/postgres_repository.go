package adapters

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"os"
)


type PostgresRepository struct {
	db *pgx.Conn
}

func (p PostgresRepository) GetPeople(ctx context.Context) ([]domain.Person, error) {
	var people []domain.Person
	query := `SELECT id, survived, pclass, name, sex, age, "siblings/spouses aboard", "parents/children abroad", fare 
              FROM people`

	rows, err := p.db.Query(ctx, query)
	defer rows.Close()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var person domain.Person

		err := rows.Scan(&person.UUID, &person.Survived, &person.PassengerClass, &person.Name, &person.Sex,
			&person.Age, &person.SiblingsOrSpousesAboard, &person.ParentsOrChildrenAboard, &person.Fare)
		if err != nil {
			return nil, err
		}
		people = append(people, person)
	}
	return people, nil
}

func (p PostgresRepository) GetPerson(ctx context.Context, uuid string) (domain.Person, error) {
	var person domain.Person
	query := `SELECT id, survived, pclass, name, sex, age, "siblings/spouses aboard", "parents/children abroad", fare 
              FROM people
	          WHERE id::text=$1;`

	err := p.db.QueryRow(ctx, query, uuid).Scan(&person.UUID, &person.Survived, &person.PassengerClass, &person.Name,
		&person.Sex, &person.Age, &person.SiblingsOrSpousesAboard, &person.ParentsOrChildrenAboard, &person.Fare)
	if err != nil {
		if err == pgx.ErrNoRows {
			return domain.Person{}, domain.InvalidPerson
		}
		return domain.Person{}, err
	}

	return person, nil
}

func (p PostgresRepository) DeletePerson(ctx context.Context, uuid string) error {
	query := `DELETE FROM people WHERE id=$1`
	_, err := p.db.Exec(ctx, query, uuid)
	if err != nil {
		if err == pgx.ErrNoRows {
			return domain.InvalidPerson
		}
		return err
	}
	return nil
}

func (p PostgresRepository) AddPerson(ctx context.Context, person domain.PersonData) (domain.Person, error) {
	query := `INSERT INTO people (id, survived, pclass, name, sex, age, "siblings/spouses aboard", "parents/children abroad", fare ) 
   			  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`

	newPerson := newPersonFromData(person)
	_, err := p.db.Exec(ctx, query, newPerson.UUID, person.Survived, person.PassengerClass, person.Name, person.Sex,
		person.Age, person.SiblingsOrSpousesAboard, person.PassengerClass, person.Fare)

	if err != nil {
		return domain.Person{}, err
	}
	return newPerson, nil

}

func (p PostgresRepository) UpdatePerson(ctx context.Context, uuid string, person domain.PersonData) (domain.Person, error) {
	query := `UPDATE people 
              SET survived=$1, pclass=$2, name=$3, sex=$4, age=$5, "siblings/spouses aboard"=$6, "parents/children abroad"=$7, fare=$8
   			  WHERE id=$9 `

	_, err := p.db.Exec(ctx, query, person.Survived, person.PassengerClass, person.Name, person.Sex, person.Age,
		person.SiblingsOrSpousesAboard, person.PassengerClass, person.Fare, uuid)

	if err != nil {
		if err == pgx.ErrNoRows {
			return domain.Person{}, domain.InvalidPerson
		}
		return domain.Person{}, err
	}

	updatedPerson := domain.Person{PersonData: person, UUID: uuid}
	return updatedPerson, nil
}

func NewPostgresRepository(con *pgx.Conn) PostgresRepository {
	return PostgresRepository{db: con}
}

func NewPostgresConnection(ctx context.Context) (*pgx.Conn, error) {
	config, err := loadConfigValues()
	if err != nil {
		return nil, err
	}

	conn, err := pgx.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func newPersonFromData(data domain.PersonData) domain.Person {
	person := domain.Person{
		PersonData: data,
		UUID:       uuid.New().String(),
	}

	return person
}

func loadConfigValues() (*pgx.ConnConfig, error) {
	port := os.Getenv("POSTGRES_PORT")
	host := os.Getenv("POSTGRES_ADDR")
	database := os.Getenv("POSTGRES_DB")
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")

	config := fmt.Sprintf("port=%s host=%s user=%s password=%s database=%s",
		port, host, user, password, database)

	parseConfig, err := pgx.ParseConfig(config)
	if err != nil {
		return nil, err
	}
	return parseConfig, nil
}