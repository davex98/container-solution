package adapters

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"sync"
)

type InMemoryRepo struct {
	people map[string]domain.Person
	lock sync.RWMutex
}

func (i *InMemoryRepo) GetPeople(ctx context.Context) ([]domain.Person, error) {
	i.lock.RLock()
	defer i.lock.RUnlock()
	var people []domain.Person
	for _, p := range i.people {
		people = append(people, p)
	}
	return people, nil
}

func (i *InMemoryRepo) GetPerson(ctx context.Context, uuid string) (domain.Person, error) {
	i.lock.RLock()
	defer i.lock.RUnlock()
	for k, p := range i.people {
		if k == uuid {
			return p, nil
		}
	}
	return domain.Person{}, domain.InvalidPerson
}

func (i *InMemoryRepo) DeletePerson(ctx context.Context, uuid string) error {
	i.lock.Lock()
	defer i.lock.Unlock()
	_, ok := i.people[uuid]
	if !ok {
		return domain.InvalidPerson
	}
	delete(i.people, uuid)
	return nil
}

func (i *InMemoryRepo) AddPerson(ctx context.Context, data domain.PersonData) (domain.Person, error) {
	newPerson := domain.Person{
		PersonData: data,
		UUID:       uuid.New().String(),
	}
	i.lock.Lock()
	defer i.lock.Unlock()
	i.people[newPerson.UUID] = newPerson
	return newPerson, nil
}

func (i *InMemoryRepo) UpdatePerson(ctx context.Context, uuid string, person domain.PersonData) (domain.Person, error) {
	i.lock.Lock()
	defer i.lock.Unlock()
	for k, p := range i.people {
		if k == uuid {
			p.PersonData = person
			i.people[k] = p
			return p, nil
		}
	}
	return domain.Person{}, domain.InvalidPerson
}

func NewInMemoryRepo() domain.Repository {
	return &InMemoryRepo{
		people: map[string]domain.Person{},
		lock: sync.RWMutex{},
	}
}
