package adapters_test

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/davex98/container-solution/internal/titanic/adapters"
	"gitlab.com/davex98/container-solution/internal/titanic/domain"
	"math/rand"
	"testing"
)

func TestPostgres(t *testing.T) {


	t.Run("Postgres test", func(t *testing.T) {
		t.Parallel()

		t.Run("Getting book", func(t *testing.T) {
			t.Parallel()
			testGettingPerson(t)
		})

		t.Run("Creating book", func(t *testing.T) {
			t.Parallel()
			testCreatingPerson(t)
		})

		t.Run("Deleting book", func(t *testing.T) {
			t.Parallel()
			testDeletingPerson(t)
		})

		t.Run("Updating book", func(t *testing.T) {
			t.Parallel()
			testUpdatingPerson(t)
		})
		t.Run("Getting book", func(t *testing.T) {
			t.Parallel()
			testGettingAllPeople(t)
		})

	})
}

func testGettingPerson(t *testing.T) {
	ctx := context.Background()

	repo := newPostgresRepo(ctx, t)
	data := domain.PersonData{
		Age:                     18,
		Fare:                    152,
		Name:                    "Kuba",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
	person, err := repo.AddPerson(ctx, data)
	require.NoError(t, err)

	p, err := repo.GetPerson(ctx, person.UUID)
	require.NoError(t, err)
	require.Equal(t, person, p)
}

func testCreatingPerson(t *testing.T) {
	ctx := context.Background()

	repo := newPostgresRepo(ctx, t)
	data := domain.PersonData{
		Age:                     18,
		Fare:                    152,
		Name:                    "Kuba",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
	person, err := repo.AddPerson(ctx, data)
	require.NoError(t, err)

	newPerson, err := repo.GetPerson(ctx, person.UUID)
	require.NoError(t, err)
	require.Equal(t, person, newPerson)
}

func testDeletingPerson(t *testing.T) {
	ctx := context.Background()

	repo := newPostgresRepo(ctx, t)
	data := domain.PersonData{
		Age:                     18,
		Fare:                    152,
		Name:                    "DELETED",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
	person, err := repo.AddPerson(ctx, data)
	require.NoError(t, err)

	err = repo.DeletePerson(ctx, person.UUID)
	require.NoError(t, err)

	oldPerson, err := repo.GetPerson(ctx, person.UUID)
	require.Equal(t, err, domain.InvalidPerson)
	require.True(t, oldPerson.IsEmpty())
}

func testUpdatingPerson(t *testing.T) {
	ctx := context.Background()

	repo := newPostgresRepo(ctx, t)
	data := domain.PersonData{
		Age:                     18,
		Fare:                    152,
		Name:                    "NEW",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
	person, err := repo.AddPerson(ctx, data)
	require.NoError(t, err)

	data.Name = "UPDATED"
	updatedPerson, err := repo.UpdatePerson(ctx, person.UUID, data)
	require.NoError(t, err)
	require.Equal(t, updatedPerson.Name, "UPDATED")
	require.NotEqual(t, updatedPerson, person)
}

func testGettingAllPeople(t *testing.T)  {
	ctx := context.Background()
	repo := newPostgresRepo(ctx, t)

	d1 := getRandomPersonData(t)
	person1, err := repo.AddPerson(ctx, d1)
	require.NoError(t, err)
	d2 := getRandomPersonData(t)
	person2, err := repo.AddPerson(ctx, d2)
	require.NoError(t, err)

	people, err := repo.GetPeople(ctx)
	require.NoError(t, err)
	require.True(t, containsPerson(people, person1))
	require.True(t, containsPerson(people, person2))
}

func getRandomPersonData(t *testing.T) domain.PersonData {
	t.Helper()
	
	return domain.PersonData{
		Age:                     rand.Float32(),
		Fare:                    0,
		Name:                    "random person",
		ParentsOrChildrenAboard: 0,
		PassengerClass:          0,
		Sex:                     "male",
		SiblingsOrSpousesAboard: 0,
		Survived:                false,
	}
}

func containsPerson(people []domain.Person, person domain.Person) bool {
	for _, p := range people {
		if p == person {
			return true
		}
	}
	return false
}

func newPostgresRepo(ctx context.Context, t *testing.T) *adapters.PostgresRepository {
	connection, err := adapters.NewPostgresConnection(ctx)
	require.NoError(t, err)

	repo := adapters.NewPostgresRepository(connection)
	return &repo
}

