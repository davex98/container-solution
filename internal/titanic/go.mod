module gitlab.com/davex98/container-solution/internal/titanic

go 1.15

require (
	github.com/deepmap/oapi-codegen v1.5.1
	github.com/go-chi/chi v1.5.2
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.2.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.9.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/davex98/container-solution/internal/common v0.0.0-20210218063348-b1a1c9bd1eb6
)
