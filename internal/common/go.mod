module gitlab.com/davex98/container-solution/internal/common

go 1.15

require (
	github.com/go-chi/chi v1.5.2
	github.com/go-chi/cors v1.1.1
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/onsi/ginkgo v1.15.0 // indirect
	github.com/onsi/gomega v1.10.5 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	google.golang.org/api v0.40.0
)
