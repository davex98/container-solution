CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


-- CREATE TABLE people
-- (
--     id                      UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
--     survived                BOOLEAN NOT NULL,
--     pclass                  INT NOT NULL,
--     name                    VARCHAR(50) NOT NULL,
--     sex                     VARCHAR(10) NOT NULL,
--     age                     INT NOT NULL,
--     "siblings/spouses aboard" INT NOT NULL,
--     "parents/children abroad" INT NOT NULL,
--     fare                    FLOAT NOT NULL
-- );
--
-- COPY people(survived, pclass, name, sex, age, "siblings/spouses aboard", "parents/children abroad", fare)
--     FROM '/data/data.csv' DELIMITER ',' CSV HEADER;

DO $$
BEGIN
    IF NOT EXISTS(SELECT *
   FROM INFORMATION_SCHEMA.TABLES
   WHERE TABLE_NAME = 'people') THEN
CREATE TABLE people
(
    id                      UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    survived                BOOLEAN NOT NULL,
    pclass                  INT NOT NULL,
    name                    VARCHAR(100) NOT NULL,
    sex                     VARCHAR(10) NOT NULL,
    age                     FLOAT NOT NULL,
    "siblings/spouses aboard" INT NOT NULL,
    "parents/children abroad" INT NOT NULL,
    fare                    FLOAT NOT NULL
);
COPY people(survived, pclass, name, sex, age, "siblings/spouses aboard", "parents/children abroad", fare)
    FROM '/data/data.csv' DELIMITER ',' CSV HEADER;
END IF;
END$$;