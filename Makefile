openapi:
	oapi-codegen -generate types -o internal/titanic/ports/openapi_types.gen.go -package ports api/openapi/swagger.yml
	oapi-codegen -generate chi-server -o internal/titanic/ports/openapi_api.gen.go -package ports api/openapi/swagger.yml

test:
	cd docker/test && docker-compose -f docker-compose.test.yml up -d

dev:
	cd docker/dev && docker-compose -f docker-compose.dev.yml up -d

build_image_prod:
	 docker build . -t titanic_app --file docker/prod/prod.Dockerfile

push_image:
ifdef titanic_version
	 docker tag titanic_app:latest jakuburghardt/container-solution:$(titanic_version) &&  docker push jakuburghardt/container-solution:$(titanic_version)
else
	@echo 'provice titanic_version, eg: make push_image titanic_version=v5'
endif