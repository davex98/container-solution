#!/bin/bash
minikube start
minikube addons enable ingress
scp -i $(minikube ssh-key) ../sql/dep.data.csv docker@$(minikube ip):/home/docker/data.csv
kubectl apply -f postgres/db-namespace.yml
kubectl apply -f postgres/db-data-volumes.yml
kubectl apply -f postgres/db-csv-volumes.yml
kubectl apply -f postgres/startscript.yml
kubectl apply -f postgres/db-secret.yml
kubectl apply -f postgres/db-deployment.yml
kubectl apply -f postgres/db-service.yml
kubectl apply -f postgres/backup/db-cronjob-secret.yml
kubectl apply -f postgres/backup/db-cronjob-backup.yml

kubectl apply -f application/app-namespace.yml
kubectl apply -f application/app-secrets.yml
kubectl apply -f application/app-config.yml
kubectl apply -f application/app-deployment.yml
kubectl apply -f application/app-server.yml

kubectl apply -f ingress/