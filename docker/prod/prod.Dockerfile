FROM golang:buster as builder
RUN update-ca-certificates
ENV USER=appuser
ENV UID=10001
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR $GOPATH/src/gitlab.com/davex98/container-solution/internal/titanic
COPY internal/titanic .
RUN go get -d -v
RUN go mod download
RUN go mod verify
RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/titanic .

FROM alpine
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/titanic /go/bin/titanic
RUN  rm -rf /var/cache/apk/*
EXPOSE 8080
USER appuser:appuser
CMD ["/go/bin/titanic"]